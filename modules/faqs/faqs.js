var $ = require('jquery');

function Faqs(el) {
  var $el = $(el);
  console.log('Accordion');

  $el.find('.faqs__item-question').on('click', function () {
    var $parent = $(this).parent();
    var $button = $(this).find('.faqs__arrow');

    var $content = $parent.find(".faqs__item-answer");
    var $contentInner = $parent.find(".faqs__item-answer-inner");
    var height = $contentInner.outerHeight();

    if ($parent.hasClass('faqs__item--active')) {
      // remove all active
      $(this).parent().removeClass('faqs__item--active');
      $content.height(0);
    } else {
      // Add active item
      $('.faqs__item').each(function () {
        $(this).removeClass('faqs__item--active');
        var $parent = $(this).parent();
        var $content = $parent.find(".faqs__item-answer");
        $content.height(0);
      });
      $(this).parent().addClass('faqs__item--active');
      $content.height(height);
    }
  });
}

module.exports = Faqs;
